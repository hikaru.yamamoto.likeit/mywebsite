CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;

Use mywebsite;

CREATE TABLE user(
id SERIAL,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
address varchar(256) NOT NULL,
password varchar(255) NOT NULL,
create_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP);

INSERT INTO user
(id,login_id,name,address,password,create_date)
VALUES(1,'admin','管理者','東京','1',now());

CREATE TABLE item(
id SERIAL,
price int(11) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
category varchar(256) NOT NULL,
detail text NOT NULL,
file_name varchar(255) NOT NULL,
user_id int(11) NOT NULL,
create_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP);

CREATE TABLE delivery(
id int(11) NOT NULL,
name varchar(256) NOT NULL,
price int(11) NOT NULL);

CREATE TABLE buy(
id int(11) NOT NULL,
user_id int(11) NOT NULL,
total_price int(11) NOT NULL,
delivery_id int(11) NOT NULL,
create_date datetime NOT NULL);

CREATE TABLE buy_detail(
id int(11) NOT NULL,
buy_id int(11) NOT NULL,
item_id int(11) NOT NULL);

CREATE TABLE board(
id SERIAL,
user_id int(11) NOT NULL,
board text NOT NULL,
item_id int(11) NOT NULL,
create_date datetime NOT NULL);


INSERT INTO user
(login_id,name,address,password,create_date)
VALUES('ad','管','東京','1',now());

INSERT INTO item(price,name,category,detail,file_name,user_id,create_date)
VALUES(1,'a','本','aaaa','a',1,now());

INSERT INTO board(user_id,board,item_id,create_date)
VALUES(1,'aaaaaaaaaaaaaaaaa',1,now());

INSERT INTO delivery (`id`, `name`, `price`) VALUES
(1, '特急配送', 500),
(2, '通常配送', 0),
(3, '日時指定配送', 200);


