package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class NewItemResult
 */
public class NewItemResult extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		String name = request.getParameter("item");
		String price = request.getParameter("price");
		String detail = request.getParameter("detail");
		String category = request.getParameter("category");
		String userId = request.getParameter("id");

		System.out.println(name+"name");
		System.out.println(price+"price");
		System.out.println(detail+"detail");
		System.out.println(category+"category");
		System.out.println(userId+"userId");

		ItemDataBeans idb = new ItemDataBeans();

		idb.setName(name);
		idb.setPrice(Integer.parseInt(price));
		idb.setDetail(detail);
		idb.setCategory(category);
		idb.setUserId(Integer.parseInt(userId));


		String confirmed = request.getParameter("confirm_button");

		switch (confirmed) {

		case "cancel":
			request.setAttribute("idb", idb);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newitem.jsp");
			dispatcher.forward(request, response);
			break;

		case "regist":
			ItemDAO.createItem(idb);
			request.setAttribute("idb", idb);
			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/newitemresult.jsp");
			dispatcher1.forward(request, response);

			break;
		}
	}
}