package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserUpdate
 */
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		UserDAO userdao = new UserDAO();
		UserDataBeans udb = userdao.userRefe(Integer.parseInt(id));

		request.setAttribute("udb",udb);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");


		UserDataBeans udb =new UserDataBeans();

		udb.setName(name);
		udb.setAddress(address);
		udb.setLoginId(loginId);
		udb.setPassword(password);

		UserDAO userdao = new UserDAO();
		UserDataBeans user = userdao.findByLoginId(loginId);

		if(user!=null||loginId.equals("")||name.equals("")||address.equals("")||password.equals("")||password1.equals("")||!password.equals(password1)) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。もしくは既に使用されているログインIDです。");

			request.setAttribute("name", name);
			request.setAttribute("address", address);
			request.setAttribute("loginId", loginId);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);

		}

			request.setAttribute("udb", udb);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdateconfirm.jsp");
			dispatcher.forward(request, response);

	}
}