package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserUpdateConfirm
 */
public class UserUpdateResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		UserDataBeans udb =new UserDataBeans();

		udb.setName(name);
		udb.setAddress(address);
		udb.setLoginId(loginId);
		udb.setPassword(password);
		udb.setId(Integer.parseInt(id));

		String confirmed = request.getParameter("confirm_button");

		switch (confirmed) {

		case "cancel":
			request.setAttribute("udb", udb);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			break;

		case "regist":
			UserDAO.userUpdate(udb);
			request.setAttribute("udb", udb);

			session.removeAttribute("userInfo");

			UserDAO userdao = new UserDAO();
			UserDataBeans user = userdao.LoginInfo(loginId, password);

			session.setAttribute("userInfo", user);

			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/userupdateresult.jsp");
			dispatcher1.forward(request, response);

			break;
		}
	}
}