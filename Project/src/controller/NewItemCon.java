package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;

/**
 * Servlet implementation class NewItemCon
 */
public class NewItemCon extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewItemCon() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("item");
		String price = request.getParameter("price");
		String detail = request.getParameter("detail");
		String category = request.getParameter("category");

		System.out.println(price + "price");

		ItemDataBeans idb = new ItemDataBeans();
		idb.setName(name);
		if ((!price.equals("")) || isNumber(price)) {
			idb.setPrice(Integer.parseInt(price));
		}
		idb.setDetail(detail);
		idb.setCategory(category);

		if (name.equals("") || price.equals("") || detail.equals("") || category.equals("")) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			request.setAttribute("idb", idb);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newitem.jsp");
			dispatcher.forward(request, response);
		}

		//		Part part = request.getPart("file");
		//
		//		String filename = this.getFilename(part);

		//		part.write(getServletContext().getRealPath(filename));

		request.setAttribute("idb", idb);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newitemconfirm.jsp");
		dispatcher.forward(request, response);
	}

	//	private String getFilename(Part part) {
	//		String pname = null;
	//		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
	//			if (dispotion.trim().startsWith("filename")) {
	//				pname = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
	//				pname = pname.substring(pname.lastIndexOf("\\") + 1);
	//				break;
	//			}
	//		}
	//		return pname;
	//
	//	}

	public static boolean isNumber(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

}
