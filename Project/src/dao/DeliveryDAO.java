package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.DeliveryDataBeans;

public class DeliveryDAO {

	//配送方法を全件取得
	public static ArrayList<DeliveryDataBeans> getAllDeliveryDataBeans() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM delivery");

			ResultSet rs = st.executeQuery();

			ArrayList<DeliveryDataBeans> deliveryDataBeansList = new ArrayList<DeliveryDataBeans>();
			while (rs.next()) {

				DeliveryDataBeans dmdb = new DeliveryDataBeans();
				dmdb.setId(rs.getInt("id"));
				dmdb.setName(rs.getString("name"));
				dmdb.setPrice(rs.getInt("price"));
				deliveryDataBeansList.add(dmdb);
			}

			System.out.println("配送方法を全件取得");

			return deliveryDataBeansList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//配送方法を配送IDで検索
	public static DeliveryDataBeans getDeliveryDataBeansByID(int DeliveryId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM delivery WHERE id = ?");
			st.setInt(1, DeliveryId);

			ResultSet rs = st.executeQuery();

			DeliveryDataBeans dmdb = new DeliveryDataBeans();
			if (rs.next()) {
				dmdb.setId(rs.getInt("id"));
				dmdb.setName(rs.getString("name"));
				dmdb.setPrice(rs.getInt("price"));
			}

			System.out.println("配送方法を配送IDで検索");

			return dmdb;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//購入IDによる配送方法検索
	public static DeliveryDataBeans getDeliveryDataBeansByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT delivery.name,"
					+ " delivery.price"
					+ " FROM buy"
					+ " JOIN delivery"
					+ " ON delivery.id = buy.delivery_id"
					+ " WHERE buy.id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			DeliveryDataBeans dmdb = new DeliveryDataBeans();

			while (rs.next()) {
				dmdb.setName(rs.getString("name"));
				dmdb.setPrice(rs.getInt("delivery.price"));

			}

			System.out.println("購入IDによる配送方法検索");
			return dmdb;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


}
