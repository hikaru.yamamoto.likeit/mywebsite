<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>フリマサイト</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="parent clearfix titlebar">
		<div class="left">
            <a href="Index">フリマサイト</a>
            </div>

            <div class="right">

		<c:if test="${userInfo.loginId==null}">
			<i>ゲストさん</i>
			<i>　</i>
        	<a href="Login">ログイン</a>
        	<i>　</i>
        	<a href="NewUser">新規登録</a>

        </c:if>

		<c:if test="${userInfo.loginId!=null}">
        	<a>${userInfo.name}さん</a>
        	<i>　</i>
            <a href="NewItem">出品</a>
            <i>　</i>
			<a href="Cart">カート</a>
			<i>　</i>
			<a href="UserList">出品者一覧</a>
			<i>　</i>
			<a href="Logout">ログアウト</a>
		</c:if>

		</div>
	</div><br><br>


        <center>
			<h4>購入詳細</h4><br>

					<table>
							<thead>
								<tr>
									<th class="center" style="width: 20%;">購入日時</th>
									<th class="center">配送方法</th>
									<th class="center" style="width: 20%">合計金額</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="center">${bdb.formatDate}</td>
									<td class="center">${bdb.deliveryName}</td>
									<td class="center">${bdb.formatTotalPrice}円</td>
								</tr>
							</tbody>
						</table>
					<br>
						<table class="bordered">
							<thead>
								<tr>
									<th class="center">商品名</th>
									<th class="center" style="width: 20%">単価</th>
								</tr>
							</thead>
						<tbody>
							<c:forEach var="ItemDataBeans" items="${List}" >
								<tr>
									<td class="center">${ItemDataBeans.name}</td>
									<td class="center">${ItemDataBeans.price}円</td>
								</tr>
									</c:forEach>
								<tr>
									<td class="center">${bdb.deliveryName}</td>
									<td class="center">${bdb.deliveryPrice}円</td>
								</tr>
							</tbody>
						</table>


            <br>
              <a href="Index">TOPページ</a>
        </center>

        <br><br>
            <footer class="page-footer brown">Made by OO</footer>
	</body>
</html>