<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>出品一覧</title>
        <link href="style.css" rel="stylesheet" type="text/css" />
</head>
	<body>
        <div class="parent clearfix titlebar">
            <div class="left">
            <a href="#">フリマサイト</a>
            </div>

            <div class="right">

        	<a href="#">ユーザ</a>

            <a href="#">出品</a>

			<a href="#">カート</a>

			<a href="#">ログアウト</a>

            </div></div>

        <center>
			<h4>出品商品詳細</h4>
						<table>
								<tr>
									<th>出品日時</th>
									<th>商品名</th>
									<th>合計金額</th>
								</tr>
								<tr>
									<td>2020/01/01</td>
									<td>商品A</td>
									<td>12345円</td>
								</tr>
						</table>
                    <br>
					   <table>
								<tr>
									<th>商品名</th>
									<th>単価</th>
								</tr>
								<tr>
									<td>商品A</td>
									<td>12345円</td>
								</tr>
								<tr>
									<td>通常配送</td>
									<td>123円</td>
								</tr>
						</table>
            <br>
               <a href="#">TOPページ</a>
        </center>

        <br><br>
            <footer class="page-footer brown">Made by OO</footer>
	</body>
</html>