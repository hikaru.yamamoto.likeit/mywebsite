<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>フリマサイト</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="parent clearfix titlebar">
		<div class="left">
            <a href="Index">フリマサイト</a>
            </div>

            <div class="right">

		<c:if test="${userInfo.loginId==null}">
			<i>ゲストさん</i>
			<i>　</i>
        	<a href="Login">ログイン</a>
        	<i>　</i>
        	<a href="NewUser">新規登録</a>

        </c:if>

		<c:if test="${userInfo.loginId!=null}">
        	<a>${userInfo.name}さん</a>
        	<i>　</i>
            <a href="NewItem">出品</a>
            <i>　</i>
			<a href="Cart">カート</a>
			<i>　</i>
			<a href="UserList">出品者一覧</a>
			<i>　</i>
			<a href="Logout">ログアウト</a>
		</c:if>

		</div>
	</div><br><br>

        <center>

         <c:if test="${errMsg != null}" ><div class="alert alert-danger"  style="color:red" role="alert">${errMsg}</div>
       		 </c:if>
          <h2>商品新規登録</h2>

	<form method="post" action="NewItemCon">
    <table class="noborder" align="center" >
        <tr>
            <th class="noborder" >商品名</th>
            <td class="noborder" ><input type="text" name="item" style="width:200px;" value="${idb.name}"></td>
        </tr>
        <tr>
            <th class="noborder" >商品価格</th>
            <td class="noborder" ><input type="text" name="price"style="width:200px;" value="${idb.price}"></td>
        </tr>
        <tr>
            <th class="noborder" >商品詳細</th>
            <td class="noborder" ><textarea name="detail" cols="30" rows="1" maxlength="80" wrap="hard"></textarea></td>
        </tr>
        <tr>
            <th class="noborder" >商品カテゴリ</th>
            <td class="noborder" ><input type="text" name="category" style="width:200px;" value="${idb.category}"></td>
        </tr>
    </table>
    <br>
         <input type="submit" value="確認"></form>
    <br>
    <br>
        <a href="javascript:history.back()" >戻る</a>
    <br>
    </center>

        <br><br>
            <footer class="page-footer brown">Made by OO</footer>
	</body>
</html>