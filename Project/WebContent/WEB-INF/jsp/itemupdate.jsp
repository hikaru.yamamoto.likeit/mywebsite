<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>フリマサイト</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="parent clearfix titlebar">
		<div class="left">
            <a href="Index">フリマサイト</a>
            </div>

            <div class="right">

		<c:if test="${userInfo.loginId==null}">
			<i>ゲストさん</i>
			<i>　</i>
        	<a href="Login">ログイン</a>
        	<i>　</i>
        	<a href="NewUser">新規登録</a>

        </c:if>

		<c:if test="${userInfo.loginId!=null}">
        	<a>${userInfo.name}さん</a>
        	<i>　</i>
            <a href="NewItem">出品</a>
            <i>　</i>
			<a href="Cart">カート</a>
			<i>　</i>
			<a href="UserList">出品者一覧</a>
			<i>　</i>
			<a href="Logout">ログアウト</a>
		</c:if>

		</div>
	</div><br><br>

        <center>
          <h2>商品情報更新</h2>

    <table class="noborder" align="center" >
        <tr>
            <th class="noborder" >商品名</th>
            <td class="noborder" ><input type="text" name="user_name" value="#"> </td>
        </tr>
        <tr>
            <th class="noborder" >商品価格</th>
            <td class="noborder" ><input type="text" name="user_address" value="#"></td>
        </tr>
        <tr>
            <th class="noborder" >商品カテゴリ</th>
            <td class="noborder" ><input type="text" name="login_id" value="#"></td>
        </tr>
        <tr>
            <th class="noborder" >商品詳細</th>
            <td class="noborder" ><input type="text" name="login_id" value="#"></td>
        </tr>

    </table>

    <p>上記の内容で変更してよろしいでしょうか？</p>

    <br>
        <input type="button" value="更新">
    <br>
    <br>
        <a href="#" >戻る</a>
    <br>
    </center>

        <br><br>
            <footer class="page-footer brown">Made by OO</footer>
	</body>
</html>